package br.com.fatecararas.biscoitodasortestarwars;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    Random random;
    int fortune, indice;

    //<editor-fold desc="DataSource Atual">
    public static String frases[][] = {
            {
                    "S01E01 - Grandes líderes inspiram grandeza nos outros."
                    , "S01E02 - Acreditar não é uma escolha, mas sim uma convicção."
                    , "S01E03 - Fácil é o caminho da sabedoria, para aqueles não cegos por si mesmos."
                    , "S01E04 - Um plano é tão bom quanto os que o realizam."
                    , "S01E05 - A melhor confiança é construída pela experiência."
                    , "S01E06 - Confie nos seus amigos, e eles terão razão pra confiar em você."
                    , "S01E07 - Apoie seus amigos, mantendo o coração mais brando do que a cabeça."
                    , "S01E08 - Heróis são feitos pelas circunstâncias."
                    , "S01E09 - Ignore seus instintos à seu próprio risco."
                    , "S01E10 - O mais poderoso é aquele que controla seu próprio poder."
                    , "S01E11 - O sinuoso caminho para a paz é sempre o mais digno, apesar das muitas voltas que ele dá."
                    , "S01E12 - Falhar com honra é melhor do que ter sucesso trapaceando."
                    , "S01E13 - A cobiça e o medo da perda são as raízes que levam até a árvore do mal."
                    , "S01E14 - Quando cercado pela guerra, escolha um lado."
                    , "S01E15 - A arrogância diminui a sabedoria."
                    , "S01E16 - A verdade ilumina a mente, mas nem sempre traz felicidade ao seu coração."
                    , "S01E17 - O medo é uma doença, a esperança é a única cura."
                    , "S01E18 - Uma única chance é uma galáxia de esperança."
                    , "S01E19 - Dura é a estrada que leva até a grandeza."
                    , "S01E20 - Os custos da guerra nunca podem ser realmente calculados."
                    , "S01E21 - O compromisso é uma virtude a ser cultivada, não uma fraqueza para ser desprezada."
                    , "S01E22 - Um segredo compartilhado é a confiança formada."
            },
            {
                    "S02E03 - A paciência é o primeiro passo para se corrigir um erro."
                    , "S02E04 - Um coração sincero nunca deve ser posto em dúvida."
                    , "S02E05 - Acredite em si mesmo ou ninguém mais acreditará."
                    , "S02E06 - Nenhum presente é mais precioso do que a confiança."
                    , "S02E08 - Apego não é compaixão."
                    , "S02E09 - Para tudo que se ganah, alguma coisa é perdida."
                    , "S02E11 - Fácil nem sempre significa simples."
                    , "S02E12 - Ao se ignorar o passado, coloca-se em risco o futuro."
                    , "S02E13 - Não tema pelo futuro, não chore pelo passado."
                    , "S02E14 - Na guerra, a verdade é a primeira vítima."
                    , "S02E15 - Buscar a verdade é fácil, aceitá-la que é difícil."
                    , "S02E16 - Um líder sábio reconhece quando deve ser liderado."
                    , "S02E17 - Coragem forja heróis, mas confiança constrói amizades."
                    , "S02E18 - Escolha o que é certo, e não o que é fácil."
                    , "S02E19 - A besta mais perigosa se encontra em nosso interior."
                    , "S02E20 - Quem meu pai foi importa menos que a minha memória dele."
                    , "S02E21E22- A adversidade é o real teste da amizade."
            },
            {
                    "S03E01E2 - Irmãos de armas são irmãos para a vida toda."
                    , "S03E03 - Onde existir força de vontade, existe um meio."
                    , "S03E04 - Uma criança roubada é uma esperança perdida."
                    , "S03E05 - O desafio da esperança é superar a corrupção."
                    , "S03E06 - Aqueles que aplicam a lei devem seguir a lei."
                    , "S03E07 - O futuro tem muitos caminhos, escolha bem."
                    , "S03E08 - Uma falha no planejamento é um plano para o fracasso."
                    , "S03E09 - O amor se apresenta em todas formas e tamanhos."
                    , "S03E10 - O medo é um grande motivador."
                    , "S03E11 - A verdade pode derrotar o espectro do medo."
                    , "S03E12 - O caminho mais rápido para destruição é a vingança."
                    , "S03E13 - Não se nasce com a maldade, ela é ensinada."
                    , "S03E14 - O caminho para o mal pode trazer grande poder, mas não lealdade."
                    , "S03E15 - Equilíbrio é encontrado naquele que enfrenta sua culpa."
                    , "S03E16 - Aquele que desiste da esperança desiste da vida."
                    , "S03E17 - Quem busca controlar o destino, nunca encontrará a paz."
                    , "S03E18 - A adaptação é a chave da sobrevivência."
                    , "S03E19 - Se algo tiver que dar errado, dará."
                    , "S03E20 - Sem honra, a vitória é vazia."
                    , "S03E21 - Sem humildade, a coragem é um jogo perigoso."
                    , "S03E22 - Um bom aluno é aquilo que o professor espera que seja."
            },
            {
                    "S04E01 - Quando o destino chama, os escolhidos precisam atender."
                    , "S04E02 - Quando o destino chama, os escolhidos precisam atender."
                    , "S04E03 - Coroas são herdadas, reinos são conquistados."
                    , "S04E04 - A aparência nem sempre diz quem realmente a pessoa é."
                    , "S04E05 - Compreender é honrar a verdade sob a superfície."
                    , "S04E06 - Quem é mais tolo, o tolo ou quem o segue?"
                    , "S04E07 - Primeiro passo até a lealdade é a confiança."
                    , "S04E08 - O caminho para ignorância é guiado pelo medo."
                    , "S04E09 - O homem sábio lidera, o homem forte segue."
                    , "S04E10 - Nossas ações definem o nosso legado."
                    , "S04E11 - Nosso objetivo sempre reflete o caminho percorrido."
                    , "S04E12 - Aquele que escraviza, inevitavelmente se torna escravo."
                    , "S04E13 - Grandes esperanças podem surgir de pequenos sacrifícios."
                    , "S04E14 - A amizade revela quem realmente somos."
                    , "S04E15 - Toda guerra é baseada na mentira."
                    , "S04E16 - Mantenha seus amigos por perto e seus inimigos mais perto ainda."
                    , "S04E17 - Os fortes sobrevivem, os nobres superam."
                    , "S04E18 - A confiança é o maior dos presentes, mas deve ser conquistada."
                    , "S04E19 - É preciso esquecer o passado para se chegar ao futuro."
                    , "S04E20 - Quem somos, nunca muda, quem pensamos que somos, sim."
                    , "S04E21 - Um inimigo caído pode se levantar de novo, mas só aquele reconciliado é vencido de verdade."
                    , "S04E22 - O inimigo do meu inimigo é meu amigo."
            },
            {
                    "S05E01 -  A FORÇA DE VONTADE PODE DERROTAR A FORÇA BRUTA"
                    , "S05E02 -  O MEDO É UMA ARMA MALEÁVEL"
                    , "S05E03 -  PROCURAR POR ALGO É ACREDITAR EM SUA EXISTENCIA"
                    , "S05E04 -  LUTAS GERALMENTE COMEÇAM E TERMINAM COM A VERDADE"
                    , "S05E05 -  A DESOBEDIÊNCIA É UM PEDIDO POR MUDANÇA"
                    , "S05E06 -  AQUELE QUE ENFRENTA A SI MESMO, DESCOBRE A SI MESMO"
                    , "S05E07 -  OS JOVENS SÃO FREQUENTEMENTE SUBESTIMADOS"
                    , "S05E08 -  QUANDO RESGATAMOS OUTROS, RESGATAMOS A NÓS MESMOS"
                    , "S05E09 -  ESCOLHA SEUS INIMIGOS COM SABEDORIA, COMO SE FOSSEM A SUA ÚLTIMA ESPERANÇA"
                    , "S05E10 -  A HUMILDADE É A ÚNICA DEFESA CONTRA A HUMILHAÇÃO"
                    , "S05E11 -  QUANDO TUDO PARECE PERDIDO UM VERDADEIRO HERÓI TRAZ A ESPERANÇA"
                    , "S05E12 -  A ARMA MAIS PODEROSA DE UM SOLDADO É A CORAGEM"
                    , "S05E13 -  É PRECISO CONFIAR EM OUTROS OU O SUCESSO É IMPOSSÍVEL"
                    , "S05E14 -  UMA VISÃO PODE TER MUITAS INTERPRETAÇÕES"
                    , "S05E15 -  AS ALIANÇAS PODEM ESCONDER AS VERDADEIRAS INTENÇÕES"
                    , "S05E16 -  A MORALIDADE SEPARA OS HERÓIS DOS VILÕES"
                    , "S05E17 -  ÀS VEZES, A MENOR DAS DÚVIDAS PODE ABALAR A MAIOR DAS CRENÇAS"
                    , "S05E18 -  A CORAGEM TEM INÍCIO NA AUTOCONFIANÇA"
                    , "S05E19 -  NUNCA FIQUE TÃO DESESPERADO A PONTO DE CONFIAR NO DESONESTO"
                    , "S05E20 -  NUNCA DESISTA DA ESPERANÇA, NÃO IMPORTA O QUÃO RUIM TUDO PAREÇA"
            },
            {
                    "S06E01 -  A VERDADE SOBRE SI MESMO É SEMPRE A PIOR DE SE ACEITAR"
                    , "S06E02 -  O SÁBIO BENEFÍCIO DE UMA SEGUNDA OPINIÃO"
                    , "S06E03 -  QUANDO TIVER DÚVIDAS, VÁ DIRETO À FONTE"
                    , "S06E04 -  A CRENÇA POPULAR NEM SEMPRE ESTÁ CORRETA"
                    , "S06E05 -  AMAR É CONFIAR. CONFIAR É ACREDITAR"
                    , "S06E06 -  O CIÚME É O CAMINHO PARA O CAOS"
                    , "S06E07 -  O ENGODO É A ARMA DA COBIÇA"
                    , "S06E08 -  SEM A ESCURIDÃO NÃO PODE HAVER LUZ"
                    , "S06E09 -  SABEDORIA ESTÁ TANTO NOS TOLOS QUANTO NOS SÁBIOS"
                    , "S06E10 -  O QUE SE É PERDIDO GERALMENTE SE É ACHADO"
            }
    };
    //</editor-fold>

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void fortuneCookie(View view) {
        random = new Random();
        indice = random.nextInt(frases.length);
        fortune = random.nextInt(frases[indice].length);
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(frases[indice][fortune]);
    }

    public void shareMessage(View view) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Biscoita da Sorte Star Wars do Dia");
        intent.putExtra(Intent.EXTRA_TEXT, frases[indice][fortune]);
        startActivity(Intent.createChooser(intent, "Escolha o App para compartilhar..."));
    }

    //<editor-fold desc="CRIACAO DE MENUS">

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.season_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.listarTemporadas) {
            Intent intent = new Intent(this,
                    SeasonListActivity.class);
            startActivity(intent);
            return true;
        } else {
            if (item.getItemId() == R.id.sair) {
                finishAffinity();
            }
            return super.onOptionsItemSelected(item);
        }
    }
    //</editor-fold>
}
