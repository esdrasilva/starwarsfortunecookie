package br.com.fatecararas.biscoitodasortestarwars;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class SeasonActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_season);

        // Obter o indice do item da lista selecionada em SeasonListActivity vindo por Intent
        int season = getIntent().getIntExtra("season",0);

        // Armazenar um array com as frases mediante o indice recebido acima
        String frases[] = MainActivity.frases[season];

        ListView listView = (ListView)findViewById(R.id.listViewSelectedSeason);

        // Criacao de Adapter de Dados para preenchimento da Lista
        ListAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,frases);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(this);
    }

    /**
     * Implementacao da Interface AdapterView.OnItemClickListener, podemos implementar
     * desta forma ao inves de criar classes aninimas.
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String message = ((TextView)view).getText().toString();

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, message);
        sendIntent.setType("text/plain");

        // Abrir WhatsApp App especificamente informando o pacote da aplicacao
        sendIntent.setPackage("com.whatsapp");
        startActivity(sendIntent);
    }
}
