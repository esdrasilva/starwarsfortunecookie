# Star Wars Fortune Cookie

Queridos Fatecanos, desta vez iremos criar um App que compartilhe a sabedoria das frases iniciais
de cada episódio das 6 temporadas de `Star Wars Clone Wars`.
Como o seriado saiu do catálogo da Netflix no dia 2 de outubro, nada melhor do que
fazermos uma pequena homenagem à saga criando um App com as features que aprendemos até o momento.

Como de praxe, crie um novo projeto, dê o nome de F290App05 ou BiscoitoDaSorteStarWars.

## Primeiro Passo
### DataSource - Nossa massa de dados
Vamos armazenar os dados com as frases de todos os episódios dentro de um Array bidimensional de Strings; os arrays facilitarão o controle dos dados devido à sua facilidade de mainulação por índices.

1. Adicione o trecho de código abaixo, acima do método onCreate, conforme a imagem abaixo.
![Imagem](images/img01.png)
```java
//<editor-fold desc="DataSource Atual">
    public static String frases[][] = {
            {
                    "S01E01 - Grandes líderes inspiram grandeza nos outros."
                    , "S01E02 - Acreditar não é uma escolha, mas sim uma convicção."
                    , "S01E03 - Fácil é o caminho da sabedoria, para aqueles não cegos por si mesmos."
                    , "S01E04 - Um plano é tão bom quanto os que o realizam."
                    , "S01E05 - A melhor confiança é construída pela experiência."
                    , "S01E06 - Confie nos seus amigos, e eles terão razão pra confiar em você."
                    , "S01E07 - Apoie seus amigos, mantendo o coração mais brando do que a cabeça."
                    , "S01E08 - Heróis são feitos pelas circunstâncias."
                    , "S01E09 - Ignore seus instintos à seu próprio risco."
                    , "S01E10 - O mais poderoso é aquele que controla seu próprio poder."
                    , "S01E11 - O sinuoso caminho para a paz é sempre o mais digno, apesar das muitas voltas que ele dá."
                    , "S01E12 - Falhar com honra é melhor do que ter sucesso trapaceando."
                    , "S01E13 - A cobiça e o medo da perda são as raízes que levam até a árvore do mal."
                    , "S01E14 - Quando cercado pela guerra, escolha um lado."
                    , "S01E15 - A arrogância diminui a sabedoria."
                    , "S01E16 - A verdade ilumina a mente, mas nem sempre traz felicidade ao seu coração."
                    , "S01E17 - O medo é uma doença, a esperança é a única cura."
                    , "S01E18 - Uma única chance é uma galáxia de esperança."
                    , "S01E19 - Dura é a estrada que leva até a grandeza."
                    , "S01E20 - Os custos da guerra nunca podem ser realmente calculados."
                    , "S01E21 - O compromisso é uma virtude a ser cultivada, não uma fraqueza para ser desprezada."
                    , "S01E22 - Um segredo compartilhado é a confiança formada."
            },
            {
                    "S02E03 - A paciência é o primeiro passo para se corrigir um erro."
                    , "S02E04 - Um coração sincero nunca deve ser posto em dúvida."
                    , "S02E05 - Acredite em si mesmo ou ninguém mais acreditará."
                    , "S02E06 - Nenhum presente é mais precioso do que a confiança."
                    , "S02E08 - Apego não é compaixão."
                    , "S02E09 - Para tudo que se ganah, alguma coisa é perdida."
                    , "S02E11 - Fácil nem sempre significa simples."
                    , "S02E12 - Ao se ignorar o passado, coloca-se em risco o futuro."
                    , "S02E13 - Não tema pelo futuro, não chore pelo passado."
                    , "S02E14 - Na guerra, a verdade é a primeira vítima."
                    , "S02E15 - Buscar a verdade é fácil, aceitá-la que é difícil."
                    , "S02E16 - Um líder sábio reconhece quando deve ser liderado."
                    , "S02E17 - Coragem forja heróis, mas confiança constrói amizades."
                    , "S02E18 - Escolha o que é certo, e não o que é fácil."
                    , "S02E19 - A besta mais perigosa se encontra em nosso interior."
                    , "S02E20 - Quem meu pai foi importa menos que a minha memória dele."
                    , "S02E21E22- A adversidade é o real teste da amizade."
            },
            {
                    "S03E01E2 - Irmãos de armas são irmãos para a vida toda."
                    , "S03E03 - Onde existir força de vontade, existe um meio."
                    , "S03E04 - Uma criança roubada é uma esperança perdida."
                    , "S03E05 - O desafio da esperança é superar a corrupção."
                    , "S03E06 - Aqueles que aplicam a lei devem seguir a lei."
                    , "S03E07 - O futuro tem muitos caminhos, escolha bem."
                    , "S03E08 - Uma falha no planejamento é um plano para o fracasso."
                    , "S03E09 - O amor se apresenta em todas formas e tamanhos."
                    , "S03E10 - O medo é um grande motivador."
                    , "S03E11 - A verdade pode derrotar o espectro do medo."
                    , "S03E12 - O caminho mais rápido para destruição é a vingança."
                    , "S03E13 - Não se nasce com a maldade, ela é ensinada."
                    , "S03E14 - O caminho para o mal pode trazer grande poder, mas não lealdade."
                    , "S03E15 - Equilíbrio é encontrado naquele que enfrenta sua culpa."
                    , "S03E16 - Aquele que desiste da esperança desiste da vida."
                    , "S03E17 - Quem busca controlar o destino, nunca encontrará a paz."
                    , "S03E18 - A adaptação é a chave da sobrevivência."
                    , "S03E19 - Se algo tiver que dar errado, dará."
                    , "S03E20 - Sem honra, a vitória é vazia."
                    , "S03E21 - Sem humildade, a coragem é um jogo perigoso."
                    , "S03E22 - Um bom aluno é aquilo que o professor espera que seja."
            },
            {
                    "S04E01 - Quando o destino chama, os escolhidos precisam atender."
                    , "S04E02 - Quando o destino chama, os escolhidos precisam atender."
                    , "S04E03 - Coroas são herdadas, reinos são conquistados."
                    , "S04E04 - A aparência nem sempre diz quem realmente a pessoa é."
                    , "S04E05 - Compreender é honrar a verdade sob a superfície."
                    , "S04E06 - Quem é mais tolo, o tolo ou quem o segue?"
                    , "S04E07 - Primeiro passo até a lealdade é a confiança."
                    , "S04E08 - O caminho para ignorância é guiado pelo medo."
                    , "S04E09 - O homem sábio lidera, o homem forte segue."
                    , "S04E10 - Nossas ações definem o nosso legado."
                    , "S04E11 - Nosso objetivo sempre reflete o caminho percorrido."
                    , "S04E12 - Aquele que escraviza, inevitavelmente se torna escravo."
                    , "S04E13 - Grandes esperanças podem surgir de pequenos sacrifícios."
                    , "S04E14 - A amizade revela quem realmente somos."
                    , "S04E15 - Toda guerra é baseada na mentira."
                    , "S04E16 - Mantenha seus amigos por perto e seus inimigos mais perto ainda."
                    , "S04E17 - Os fortes sobrevivem, os nobres superam."
                    , "S04E18 - A confiança é o maior dos presentes, mas deve ser conquistada."
                    , "S04E19 - É preciso esquecer o passado para se chegar ao futuro."
                    , "S04E20 - Quem somos, nunca muda, quem pensamos que somos, sim."
                    , "S04E21 - Um inimigo caído pode se levantar de novo, mas só aquele reconciliado é vencido de verdade."
                    , "S04E22 - O inimigo do meu inimigo é meu amigo."
            },
            {
                    "S05E01 -  A FORÇA DE VONTADE PODE DERROTAR A FORÇA BRUTA"
                    , "S05E02 -  O MEDO É UMA ARMA MALEÁVEL"
                    , "S05E03 -  PROCURAR POR ALGO É ACREDITAR EM SUA EXISTENCIA"
                    , "S05E04 -  LUTAS GERALMENTE COMEÇAM E TERMINAM COM A VERDADE"
                    , "S05E05 -  A DESOBEDIÊNCIA É UM PEDIDO POR MUDANÇA"
                    , "S05E06 -  AQUELE QUE ENFRENTA A SI MESMO, DESCOBRE A SI MESMO"
                    , "S05E07 -  OS JOVENS SÃO FREQUENTEMENTE SUBESTIMADOS"
                    , "S05E08 -  QUANDO RESGATAMOS OUTROS, RESGATAMOS A NÓS MESMOS"
                    , "S05E09 -  ESCOLHA SEUS INIMIGOS COM SABEDORIA, COMO SE FOSSEM A SUA ÚLTIMA ESPERANÇA"
                    , "S05E10 -  A HUMILDADE É A ÚNICA DEFESA CONTRA A HUMILHAÇÃO"
                    , "S05E11 -  QUANDO TUDO PARECE PERDIDO UM VERDADEIRO HERÓI TRAZ A ESPERANÇA"
                    , "S05E12 -  A ARMA MAIS PODEROSA DE UM SOLDADO É A CORAGEM"
                    , "S05E13 -  É PRECISO CONFIAR EM OUTROS OU O SUCESSO É IMPOSSÍVEL"
                    , "S05E14 -  UMA VISÃO PODE TER MUITAS INTERPRETAÇÕES"
                    , "S05E15 -  AS ALIANÇAS PODEM ESCONDER AS VERDADEIRAS INTENÇÕES"
                    , "S05E16 -  A MORALIDADE SEPARA OS HERÓIS DOS VILÕES"
                    , "S05E17 -  ÀS VEZES, A MENOR DAS DÚVIDAS PODE ABALAR A MAIOR DAS CRENÇAS"
                    , "S05E18 -  A CORAGEM TEM INÍCIO NA AUTOCONFIANÇA"
                    , "S05E19 -  NUNCA FIQUE TÃO DESESPERADO A PONTO DE CONFIAR NO DESONESTO"
                    , "S05E20 -  NUNCA DESISTA DA ESPERANÇA, NÃO IMPORTA O QUÃO RUIM TUDO PAREÇA"
            },
            {
                    "S06E01 -  A VERDADE SOBRE SI MESMO É SEMPRE A PIOR DE SE ACEITAR"
                    , "S06E02 -  O SÁBIO BENEFÍCIO DE UMA SEGUNDA OPINIÃO"
                    , "S06E03 -  QUANDO TIVER DÚVIDAS, VÁ DIRETO À FONTE"
                    , "S06E04 -  A CRENÇA POPULAR NEM SEMPRE ESTÁ CORRETA"
                    , "S06E05 -  AMAR É CONFIAR. CONFIAR É ACREDITAR"
                    , "S06E06 -  O CIÚME É O CAMINHO PARA O CAOS"
                    , "S06E07 -  O ENGODO É A ARMA DA COBIÇA"
                    , "S06E08 -  SEM A ESCURIDÃO NÃO PODE HAVER LUZ"
                    , "S06E09 -  SABEDORIA ESTÁ TANTO NOS TOLOS QUANTO NOS SÁBIOS"
                    , "S06E10 -  O QUE SE É PERDIDO GERALMENTE SE É ACHADO"
            }
    };
    //</editor-fold>
```
### Segundo Passo - Criação de Layout
1. Vamos criar o Layout da Atividade Principal. Crie a árvore de componentes conforme a imagem abaixo e crie as constraints para os dois ImageViews que representas os sóis de Tatooine.
![ScreenShot 01](images/img02.png)

2. Confira o XML da Activity.

```xml
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <ImageView
        android:id="@+id/imageView"
        android:layout_width="411dp"
        android:layout_height="wrap_content"
        android:adjustViewBounds="false"
        android:cropToPadding="false"
        android:scaleType="centerCrop"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:srcCompat="@drawable/fortune_cookie_bg" />

    <ImageView
        android:id="@+id/imageViewMessage"
        android:layout_width="68dp"
        android:layout_height="75dp"
        android:layout_marginStart="52dp"
        android:layout_marginTop="60dp"
        android:onClick="fortuneCookie"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:srcCompat="@drawable/jedi_order_white" />

    <ImageView
        android:id="@+id/imageViewShare"
        android:layout_width="67dp"
        android:layout_height="60dp"
        android:layout_marginStart="8dp"
        android:layout_marginTop="56dp"
        android:onClick="shareMessage"
        app:layout_constraintStart_toEndOf="@+id/imageViewMessage"
        app:layout_constraintTop_toTopOf="@+id/imageViewMessage"
        app:srcCompat="@drawable/share_action" />

    <TextView
        android:id="@+id/textView"
        android:layout_width="200dp"
        android:layout_height="wrap_content"
        android:layout_marginStart="52dp"
        android:layout_marginTop="124dp"
        android:text="@string/forca"
        android:textColor="#000000"
        android:textSize="14sp"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/imageViewShare" />
</android.support.constraint.ConstraintLayout>

```

### Terceiro Passo - Codificação do Métodos de Interação
1. Vamos criar o método que gera randomicamente a seleção de uma das frases em nosso `Datasource`.
![ScreenShot 03](images/img03.png)

2. Associe a função ao imageView para executar a função que realiza a seleção de frase cnforme a imgegem abaixo.
![](images/img04.png)

3. Vamos criar o método com a `Intent` para compartilhar as frases do nosso **Biscoito da Sorte**.
Adicione o código da função abaixo.
![ScreenShot 04](images/img05.png)

4. Associe a função ao ImageView para compartilhamento.
![ScreenShot 05](images/img06.png)

### Quarto Passo - Criação de Acitivies utilizando Listas
Neste passo ao invés de sortear as frases do **Biscoito da Sorte**, vamos criar uma lista contendo as temporadas da série **Clone Wars**, e ao selecionar uma temporada todas as frases da temporada selecionada serão exibidas ao usuário.
Para tal, iremos utilizar o componente `ListView`.

#### SeasonListActivity
1. Crie uma nova Activity `SeasonListActivity` e adicione o widget `ListView` no centro e configure as constrainst conforme a imagem abaixo.
![ScreenShot 06](images/img07.png)
2. Adicione a lista de temporadas no recurso `string.xml`. Esta Activity será nosso _menu_ de seleção de temporadas.
```xml
<resources>
    <string name="app_name">BiscoitoDaSorteStarWars</string>
    <string name="forca">Que a força esteja com você...</string>
    <string-array name="season_list">
        <item>Primeira Temporada</item>
        <item>Segunda Temporada</item>
        <item>Terceira Temporada</item>
        <item>Quarta Temporada</item>
        <item>Quinta Temporada</item>
        <item>Sexta Temporada</item>
    </string-array>
</resources>
```
3. Codificação. Adicione o código abaixo à Activity `SeasonListActivity`.
```java
public class SeasonListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_season_list);

        ListView listView = (ListView)findViewById(R.id.listViewSeasons);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getApplicationContext(),SeasonActivity.class);
                // Inserir posicao da celula selecionada para SeasonActivity
                intent.putExtra("season",position);
                startActivity(intent);
            }
        });
    }
}
```

#### SeasonActivity
Vamos criar a Activity que logo após a seleção de temporada, exibirá as frase de cada temporada selecionada. Para tal precisamos qual temporada foi selecionada na Activity anterior e resgatar o arrays estático (nosso **DataSource** na MainActivity) para poder preencher os elementos em nossa `SeasonActivity`.

1. Crie uma nova Activity `SeasonActivity` e adicione uma `ListView` no centro e ajuste as constraints.
![](images/img08.png)

#### Codificação
Agora precisamos criar o código necessário para exibir as frases de uma temporada selecionada, sendo esta recebida de `SeasonListActivity`.

1. Codificação. Adicione o código abaixo à Activity.
```java
public class SeasonActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_season);

        int season = getIntent().getIntExtra("season",0);

        String frases[] = MainActivity.frases[season];

        ListView listView = (ListView)findViewById(R.id.listViewSelectedSeason);

        ListAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,frases);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String message = ((TextView)view).getText().toString();

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, message);
        sendIntent.setType("text/plain");

        // Abrir WhatsApp App especificamente
        sendIntent.setPackage("com.whatsapp");
        startActivity(sendIntent);
    }
}
```
### Quinto Passo - Criação de Menus
Vamos criar um **Menu** para alterar o fluxo de navegação fazendo com que a seleção das temporadas através das Listas sejam iniciadas a partir de um menu.
1. Crie um diretório especial para armazenar os menus. Sobre o diretório `res`, com o clique direto, selecione esta opção
![](images/img09.png)
2. Sobre o diretório `menu` crie um novo recurso.
![](images/img10.png)
3. Adicione os itens de menu ao arquivo `menu.xml`.
```xml
<?xml version="1.0" encoding="utf-8"?>
<menu xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:id="@+id/listarTemporadas" android:title="Temporadas" />
    <item android:id="@+id/sair" android:title="Sair"/>
</menu>
```


### Sexto Passo - Codificação das Ações do Menu
Agora vamos implementar a transição entre as Telas através do `menu`.
1. Na classe `MainActivity`, adicione o treco de código abaixo.
```java
//<editor-fold desc="CRIACAO DE MENUS">

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.season_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.listarTemporadas) {
            Intent intent = new Intent(this,
                    SeasonListActivity.class);
            startActivity(intent);
            return true;
        } else {
            if (item.getItemId() == R.id.sair) {
                finishAffinity();
            }
            return super.onOptionsItemSelected(item);
        }
    }
    //</editor-fold>
```
# Vamos Testar
